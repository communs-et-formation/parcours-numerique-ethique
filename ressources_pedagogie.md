# Ressources utiles

## Mooc

2 Mooc de la GEN : 

- [Pédagogie active](https://www.grandeecolenumerique.fr/mooc-embarquez-vos-apprenants-destination-pedagogie-active)
- [Evaluation](https://www.grandeecolenumerique.fr/mooc-evaluez-efficacement-pour-faire-progresser-vos-apprenants)

Lors du Mooc Pédagogie active, 4 webinaires ont eu lieu, voici le lien des replays :
- [L'évolution du métier d'enseignant / formateur](https://www.youtube.com/watch?v=Akl9HDSAppQ)
- [La pédagogie active dans les formations pour adultes](https://www.youtube.com/watch?v=QJvnAXpPhL8)
- [La multimodalité en formation](https://www.youtube.com/watch?v=8tsTxKoDshc)
- [Gamification & Nudge](https://www.youtube.com/watch?v=ueKTwos5vmU)

# Ressources utiles

## Mooc

2 Mooc de la GEN : 

- [Pédagogie active](https://www.grandeecolenumerique.fr/mooc-embarquez-vos-apprenants-destination-pedagogie-active)
- [Evaluation](https://www.grandeecolenumerique.fr/mooc-evaluez-efficacement-pour-faire-progresser-vos-apprenants)

Lors du Mooc Pédagogie active, 4 webinaires ont eu lieu, voici le lien des replays :
- [L'évolution du métier d'enseignant / formateur](https://www.youtube.com/watch?v=Akl9HDSAppQ)
- [La pédagogie active dans les formations pour adultes](https://www.youtube.com/watch?v=QJvnAXpPhL8)
- [La multimodalité en formation](https://www.youtube.com/watch?v=8tsTxKoDshc)
- [Gamification & Nudge](https://www.youtube.com/watch?v=ueKTwos5vmU)

## Prise de notes / idées

- Démarrez par un questionnaire pour comprendre les besoins et aider les participants à conscientiser leur démarche
- Proposer à l'étudiant de définir/poser son environnement pédagogique : plage horaire, ressources, matériel, lieu, partenaires, ambitions... [exemple pdf](environnement_personnel.pdf), [mindmap](environnement_apprentissage.jpeg) ou [article](https://www.unow.fr/blog/digital-learning-et-formation/conseils-construire-environnement-personnel-apprentissage/)
- Profil de l'apprenant moderne : ![Profile](image.png)
- Critère de l'apprentissage pour adultes : ![Critères](image-1.png)
- Exemple de questionnaire :

Leur formation : 

    De quand date leur dernière formation ?
    Sont-ils capables de s’autoévaluer avec une grille critériée d’évaluation ?
    Préfèrent-t-ils travailler seul ou en groupe ?

Leurs motivations :

    Quels sont leurs objectifs ?
    Quels sont leurs points forts/faibles ?
    Dans quelle situation se projettent-ils dans 5 ou 10 ans ?
    Qu’attendent-ils de cette formation ?

- les motivations à se former : [article](https://blog.betterstudy.ch/motivations-former)
- motivation au travail : [article](https://opensourcing.com/blog/motivations-travail/)
- multimodalité : [glossaire](https://view.genial.ly/61de9a28d845d90de87d090b/interactive-content-concepts-pedagogie)
![Concepts](image-2.png)
- mise en oeuvre de pédagogies actives : ![exemples](image-3.png)
- Pédagogie active Université de louvain [PDF](pedago_active.pdf)
- Multimodalité : [PDF](multimodalite.pdf)
- Guide méthodologique : https://view.genial.ly/614c26f16a2ae70d4d47e1e3
- Besoins, roues, verbes : https://blogs.univ-poitiers.fr/t-roy/2014/05/09/taxonomie-de-bloom-et-roue-padagogique/

- Formations multimodales :
    https://travail-emploi.gouv.fr/IMG/pdf/formations_multimodales.pdf

https://uclouvain.be/fr/etudier/lll/les-cahiers-du-louvain-learning-lab.html


**Contenu dispo :  **
- https://luniversitenumerique.fr/ressources/fun-ressources/ (Ressources / transition ecolo)
- https://grow.google/intl/fr_fr/courses-and-tools/
- https://www.edx.org/
- https://www.my-mooc.com/fr/
- https://www.fun-mooc.fr/fr/

** Outils **
- https://infogram.com/fr
- https://www.visme.co/fr/
- https://miro.com/fr/
- https://www.wooclap.com/fr/
- https://genial.ly/fr/home/
- https://nuagedemots.co/
