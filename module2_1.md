*Ceci est un module "test"*

# Module 2.1
## Le numérique pour la transition écologique ? 

### Intro

Le numérique est-il un gouffre qui engloutit et détruit nos ressources naturelles ou le meilleur allié de la transition ? Evidemment, posée comme cela, la question semble bien trivale (et absurde !). Rien n'est jamais si simple et à question complexe, réponse complexe... Enfin pas forcément ! La première étape face à un dilemne aussi cornélien, c'est d'abord de se poser des questions, d'évaluer nos actions, de faire des bilans et ensuite d'ajuster. En bref, c'est une problématique comme une autre, la méthode ne doit pas vous être inconnue... Mais le numérique a pris une telle place dans notre "environnement" quotidien et la question de l'"environnement", naturel, climatique, et social cette fois-ci, représente un tel enjeu pour les décennies à venir que cela mérite surement que l'on y prête une attention particulière.
Commençons pour regarder de plus près la situation...

#### Le numérique pollue
Oui, c'est un fait ! Comme toute activité humaine, l'usage du numérique a un impact sur notre environnement. Il nécessite du matériel, qui demande des ressources naturelles et génère de déchets, il consomme de l'énergie. De plus les appareils numériques sont tellement omniprésent, que même si chacun d'entre eux consommait peu d'énergie ou générait peu de déchêts, cela représenterait déjà un impact important pour notre environnement. L'autre difficulté est que l'on y prête quasiment plus attention. Notre téléphone est allumé en permanence, rechargé tous les jours, l'ordinateur fait partie du quotidien d'une grande partie de la population. Et ce n'est que la partie "visible". Pour maintenir connecté tous ces appareils, il faut énormément d'infrastructures : des serveurs, des routeurs, des antennes... La technologie 4G qui arrive à votre téléphone n'a peut-être pas de "présence physique", elle est néanmoins construite sur un tas d'infrastructures consommatrices de ressources.
Rendez-vous compte, si Internet était un pays, il serait le 3ème plus gros consommateur d’électricité au monde avec 1500 TWH par an, derrière la Chine et les Etats-Unis. Au total, le numérique consomme 10 à 15 % de l’électricité mondiale, soit l’équivalent de 100 réacteurs nucléaires. Et cette consommation double tous les 4 ans ! Selon le chercheur Gerhard Fettweis, la consommation électrique du web atteindrait en 2030 la consommation mondiale de 2008 tous secteurs confondus. Dans un futur proche, Internet deviendrait ainsi la première source mondiale de pollution. 
Allez, on ne va pas vous bombarder de chiffre, vous avez surement déjà en tête tout cela. Mais cela ne fait pas de mal de le rappeler... et d'en parler autour de soit. Vous pourriez être surpris de la méconnaissance de la question par vos interlocuteurs...
Et si vous voulez rentrer un peu plus dans le détails, voici quelques sources :
[https://www.fournisseur-energie.com/internet-plus-gros-pollueur-de-planete/#:~:text=Si%20Internet%20était%20un%20pays,double%20tous%20les%204%20ans%20!]

#### Le numérique aide à réduire notre impact écologique
Voilà un autre fait ! 
- limitation des déplacements, télétravail, collaboration et partage de solution pour la transition...

#### Efficacité versus efficience

#### Et si on devait faire sans ?
En cas de black-out ou de restriction, pourait-on faire sans le numérique ? Comment intégrer la résilience dans nos pratiques numériques ?

### Dans la "vraie vie", ça veut dire quoi ?

Un seul exercice pour ce module... mais il va vous demander de l'implication !

#### Le Batterie Challenge !
Pendant une semaine :
- je recharge à bloc mon téléphone le soir pour démarrer la journée à 100% !
- tous les soirs je fais une capture d'écran de mon téléphone qui rend visible mon niveau de batterie
- ok vous avez tous des téléphones différents, on ne va pas comparer vos niveaux de batterie, mais le challenge est de tout faire pour que chaque jour, votre niveau de batterie en fin de journée soit le plus haut possible. 
- tous les jours, vous tenez un journal des moments où vous vous êtes retenu d'utiliser votre téléphone : pour quelle activité, qu'avait vous fait à la place, est-ce que cela a été génants ou au contraire bénéfique.

A la fin de la semaine, lister toutes les difficultés que vous avez rencontré en n'utilisant pas votre téléphone, et utilisez le numérique :-) pour trouver des solutions ! Le numérique pour se passer du numérique, marant non ?

### Ok, donc ce que je retiens, c'est...
- Le numérique est un gros consommateur d'énergie et de ressources naturels. Ca ne fait pas de mal d'y repenser régulièrement !
- Le numérique est un des solutions qui m'aider à réduire mon impact écologique
- Face à ce paradoxe, j'apprends à raisonner mes pratiques numériques pour qu'elle ait un "bilan" positif. Bref, je suis efficient !

### Travail : ajouter au glossaire collaboratif
- efficience

### Travail : Rédiger et partager au groupe votre note d'étonnement
Pas pour ce module, c'est le challenge qui fait office d'exercice et de note d'étonnement

### Et si je veux aller un peu plus loin ?

#### A lire / à voir sur le web
- [La face cachée du numérique](https://librairie.ademe.fr/cadic/4932/guide-pratique-face-cachee-numerique.pdf?modal=false) - Publié par l'ADEME
