# Nom du parcours

Ecologie & Numérique : Découvrir et s'inspirer de la démache low tech pour le projet et les actions du centre social

# Decription synthétique


Un parcours de formation en présentiel et à distance pour s'approprier les principes-clés de la démarche low tech et s'en inspirer pour favoriser la réponse aux besoins des habitants en privilégieant des pratiques et des solutions "utiles, accessibles et durables".

Formation proposée par les Centres Sociaux Connectés du Nord Pas-de-Calais
et conçue en collaboration avec Pierre Trendel / Optéos

# Public cible

Cette formation s'adresse aux professionnel.le.s des centres sociaux fédérés du Nord Pas-de-Calais : Fonctions d'animation, de coordination, de direction... quel que soit le secteur (numérique, famille-parentalité, enfance-jeunesse...)

# Pré-requis

Capacité à suivre des modules de formation à distance en autonomie (appropriation du contenu et réalisation d'exercices)

# Format
5 ou 6 journées de formation : 2 (ou 3) jours en présentiel et 2 (ou 3) jours en FOrmation A Distance (FOAD).


# Dates

Avril à juin 2023

# Prise en charge
