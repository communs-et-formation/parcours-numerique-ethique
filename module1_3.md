*Ceci est un module "test"*

# Module 1.3
## Le DIY : Faire soi-même (mais avec les autres !) 

### Intro


### Dans la "vraie vie", ça veut dire quoi ?


### Ok, donc ce que je retiens, c'est...

### Travail : ajouter au glossaire collaboratif
- circuit-court

### Travail : Rédiger et partager au groupe votre note d'étonnement
Une note d'étonnement est un note dans laquelle un nouvel entrant (il s'agit souvent d'entreprise car cette méthodologie est issu du monde RH) décrit quelque chose qui l'a supris lors de ses premiers jours et apporte une vision et une critique constructive de la situation.
Ici vous devez choisir un point qui dans ce module vous a surpris (quelque chose que vous ne connaissiez pas, auquel vous n'aviez jamais pensé...) et de le partager en quelques lignes avec le reste du groupe.
### Et si je veux aller un peu plus loin ?

#### A lire / à voir sur le web


