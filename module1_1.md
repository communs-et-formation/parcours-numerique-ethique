*Ceci est un module "test"*

# Module 1.1
## Des technologies pour tous 

### Intro

Les nouvelles technologies sont de formidables outils qui rendent de nombreux services au quotidien et qui aident à concevoir des solutions à de nombreux problèmes. A condition evidemment d'être en capacité de les utiliser et qu'elles ne créent pas plus de problèmes que de solutions !

A mesure, que les technologies évoluent, elles s'enrichissent mais aussi se compléxifient. Elles se succèdent et chaque nouvelle "couche" nécessite de nouveaux apprentissages, la compréhension de nouvelles manières de faire, un nouveau vocabulaire...

Si l'on y fait pas attention, rater une simple marche peut vite mener à rester "en bas de l'escalier"...

Pourtant si l'on souahite que le numérique soit un facteur de transformation (sociale, écologique, économique ...), il faut bien faire attention à embarquer tout le monde. Sans quoi ce que peut nous apporter le numérique et la technologie d'une manière générale ne pourrait être qu'une promesse.

#### De l'exclusion à l'inclusion numérique

L'*exclusion numérique* est une thématique préoccupante. Et elle concerne potentiellement tout le monde car la fracture avec le monde technologique peut survenir pour tout un tas de raisons et à différents moments de la vie : par la survenue d'une situation de handicap, l'âge, un "décrochage" personnel, ou tout simplement une inégalité d'accès liée à une situation géographique.

Pensez un "numérique pour tous" est évidemment une tâche d'une grande ampleur, mais il resulte surtout d'une vigilance et d'une prise de considération de la diversité des usages et des usagers dans la conception des technologies et des dispositifs qui les mettent en oeuvre.

Parfois tout simplement l'utilisation d'un meilleur design, l'"allégement" des fonctionnalités superflues, le poids d'un fichier, la traduction, la conformité aux consignes d'accessibilité, etc.. suffisent à déplacer la frontière entre une technologie qui exclue et d'une technologie qui rapproche.

##### A lire : 
L'article ["Quand les français éprouvent des difficultés face aux numérique"](https://mydigitalweek.com/quand-les-français-eprouvent-des-difficultes-face-au-numerique/)
et l'[étude](https://www.ifop.com/wp-content/uploads/2022/04/PPT_Simplon_2022.03.07.pdf) relative.

### Dans la "vraie vie", ça veut dire quoi ?

#### Les services publics

L'accès au services publics est un enjeu majeur de démocratie. Depuis plusieurs années, nombreux services sont passés ou sont en cours de passage au numérique. Cela simplifie la vie des usagers, accèlère les temps de traitement... Du moins si on est en capacité technique et intellectuelle d'y accéder.

##### Exercice (à partager avec le groupe)
Prenez un service public numérique, et mettez-vous en situation d'usage en vous mettant successivement dans différentes situations :
- en situation d'handicap
- dans une langue étrangère (utiliser la traduction automatique du navigateur)
- une autre situation que vous aimeriez tester

Notez vos commentaires sur les 4 points suivants et partagez le avec le groupe (exercice avec évaluation par les pairs)

Et posez vous les questions suivantes : 
- [ ] Comment faire  : est-ce tout simplement possible, quelles sont les difficultés ?
- [ ] Comment faire sans ou autrement : existe-t-il des alternatives ?

**Inspiration:**
L'application [Refugeye](https://refugeye.com) permet d'échanger avec les autorités, services publics, ONG, ... en utilisant un système de dessin et de pictogramme pour se faire comprendre auprès de son interlocuteur.

#### Le numérique en réponse à une situation de handicap : Be my eyes

[Be my eyes](https://www.bemyeyes.com/language/french) est une application mobile qui permet aux déficients visuels de faire appel à une communauté de bénévole pour résoudre des difficultés du quotidien. En installant l'application, on est alors susceptible d'être contacter en appel vidéo sur son smartphone pour simplement aider un bénéficiaire à choisir un vêtement, vérifier une date de péremption sur un produit...

##### Exercice (à partager avec le groupe)
Regardez la [vidéo de présentation](https://youtu.be/6GRfFuWsjNU) de l'application.
Le système d'aide proposé par l'application est la mise en relation d'un grand groupe de personne afin de résoudre une difficulté dans des délais courts. C'est une forme de *crowdsourcing*.

A partir d'une situation vécue dans votre environnement : 
- [ ] Décrivez brièvement la situation
- [ ] Imaginez une solution mettant en oeuvre une forme crowdsourcing numérique

#### Apprendre le numérique sans numérique
La société [COLORI](https://colori.fr) a créer un programme d'appropriation du numérique à destination des 3-8 ans ... sans utiliser le numérique. Les compétences nécessaires pour bien évoluer dans un monde "numérique" sont aussi des compétences que l'on peut développer dans le monde "réel".

Parcourez le site internet de l'entreprise et :
- [ ] Repérer à travers une activité comment la compétence numérique est acquise sans le numérique (quel moyen pédagogique, quel type de matériel)
- [ ] Imaginez comment adapter ces activités à un public adulte
- [ ] et/ou imaginez une activité formatrice à un usage numérique... sans numérique


### Ok, donc ce que je retiens, c'est...

- [ ] Le numérique est une source de transformation sociale, écologique, économique à condition que les technologies utilisées soit accessible à tous
- [ ] Une solution technologique (et non technologique évidemment) amène un bénéfice mais aussi un risque. La bonne conception de ces outils est une clé de son pouvoir de transformation (ou de "nuisance").
- [ ] Une solution numérique n'est pas forcément numérique :-)

### Travail : ajouter au glossaire collaboratif
- Exclusion numérique
- Crowdsourcing

### Travail : Rédiger et partager au groupe votre note d'étonnement
Une note d'étonnement est un note dans laquelle un nouvel entrant (il s'agit souvent d'entreprise car cette méthodologie est issu du monde RH) décrit quelque chose qui l'a supris lors de ses premiers jours et apporte une vision et une critique constructive de la situation.
Ici vous devez choisir un point qui dans ce module vous a surpris (quelque chose que vous ne connaissiez pas, auquel vous n'aviez jamais pensé...) et de le partager en quelques lignes avec le reste du groupe.

### Et si je veux aller un peu plus loin ?

#### A lire / à voir sur le web
- [Accessibilité pour le web](https://www.w3.org/WAI/fundamentals/accessibility-intro/fr)
- [OLPC : one laptop per child](https://laptop.org)
- [Emmaus connect](https://emmaus-connect.org)
- [Tarifs sociaux mobile et internet](https://www.ariase.com/box/actualite/tarifs-premiere-necessite-box-forfait-mobile)
- [Universal design (en anglais)](https://bootcamp.uxdesign.cc/universal-design-the-need-for-assistive-accessible-technology-63090b452cc1)
- [Niveau de langage recommandé pour l'accessibilite] ref à retrouver
