# Pistes de travail pour l'expérimentation

## Contexte

Dans le cadre de la création d'un module de formation autour de la transition numérique éthique et des low-tech, on cherche à expérimenter un format de formation hybrique (blended learning) adapté au sujet et au contexte particulier des Centres sociaux.

## Intention de l'exploration

Créer un parcours de formation qui :
- donne les clés de lecture et de compréhension des enjeux liés la transition écologique et sociale autour des outils et dispositifs numériques
- favorise la prise d'initiative d'acteurs de proximité pour intégrer ces thématiques dans leurs animations, évènements, actions auprès des publics
- initie une démarche collaborative d'échange et de travail sur la constitution de nouveaux formats et d'une documentation partagée


## Les axes

### Axe "Ingénieurie pédagogique"

#### Blended learning et formation en autonomie

Action : 
- Créer des formations modulaires qui alterne temps présentiel et distanciel ainsi qu'une large partie de travail en autonomie
- Evaluer les différents ratios de durée entre les différentes modalités pour optimiser le temps de formation et l'assimilation des contenus en maintenant l'implication du stagiaire
- Définir le cadre contractuel à conclure avec la structure du stagiaire pour lui garantir le temps, les moyens et la disponibilité nécessaire au suivi de la formation (sanctuarisation d'une plage horaire, salle dédiée, regroupement de stagiaire...) 
- Etudier la possibilité de suivre la formation en binôme animateur/usager pour intégrer les différents points de vue
- Définir la posture du formateur, son rôle dans l'accompagnement des stagiaires et les formes prises par ses interventions

Livrable/indicateur : 
- Trame pédagogique sur la base d'une formation de 4 jours décrivant le séquençage des différentes modalités et la diffusion des ressources pédagogiques fournies au stagiaire
- Evaluation à chaud (J+1) et à froid (J+30) des stagiaires 
- Rapport d'évaluation du formateur

#### Pédagogie inversée en ligne

Action : 
- Concevoir une séquence type d'apprentissage basée sur des apports en ressources pédagogiques et la réalisation d'exercices accessibles en ligne en amont des temps de présentiel/distanciel avec le formateur
- Choix et design des ressources pédagogiques et des exercices à utiliser en autonomie
- Modalité de conduite du temps "formateur" : quelle méthodologie selon la modalité distancielle/présentielle ?
- Déterminer les éléments d'évaluation à chaud de l'acquisition des notions du module 

Livrable/indicateur : 
- Un module type de ressources pédagogique
- Une sélection d'exercices types adaptés à la modalité
- Un conducteur pédagogique d'animation du "temps formateur" en conclusion du module (modalité distancielle)

#### Pédagogie active-collaborative

Action : 
- Impliquer et accompagner les stagiaires dans la co-rédaction d'une documentation propre à la session de formation qui constitue une extension au matériel pédagogique fourni et qui puisse être enrichie au fur et à mesure des sessions
- Adapter les formats d'exercices et les ressources pédagogiques pour inciter les stagiaires à produire cette documentation
- Favoriser la constitution d'une documentation collaborative réalisée par l'ensemble des stagiaires

Livrable/indicateur : 
- Une sélection d'exercices qui favorise cette pratique
- Une sélection d'outils qui permettent la rédaction collaborative de cette documentation

### Axe "Evaluation"

#### Auto-évaluation et évaluation par les pairs

Action : 
- Développer les formats pédagogiques distanciels qui intègrent un premier temps d'auto-évaluation suivi par un temps d'évaluation par les pairs
- Définir le rôle et la posture spécifique du formateur dans la restitution des évaluations

Livrable/indicateur : 
- Une sélection d'exercices qui favorise cette pratique
- Une grille d'auto-évaluation et d'évaluation de ses pairs à destination des stagiaires
- Un conducteur pédagogique d'animation du "temps formateur" sur la restitution des travaux

#### Certification Open Badge

Action : 
- Evaluer l'opportunité de lier chaque module (ou partie d'un module) à un Open Badge 
- Définir la bonne "granularité" des badges et les modalités d'acquisition du badge

Livrable/indicateur : 
- Un badge type 

#### Rattachement aux référentiels nationaux/internationaux

Action : 
- Lister et identifier les référentiels rattachés à la thématique et examiner les possibilités de validation partielle ou totale de ces référentiels par les stagiaires
Livrable/indicateur : 
- Liste des référentiels et items "validables" au sein de ces référentiels. 
- identifier les possibilités de suite de formation pour valider intégralement les référentiels


### Axe "La formation comme un commun"

#### Diffusion, partage et collaboration sur les programmes

Action : 
- Réaliser et mettre à disposition le matériel pédagogique sous un format et une licence qui permettent leur utilisation et leur enrichissement par une structure tierce
- Déterminer les conditions nécessaires à la constitution d'une communauté pédagogique autour du parcours
Livrable : 
- Etude et recueil de stratégie similaire dans le domaine de la formation ou ailleurs
- Liste des points critiques à intégrer à un parcours pour rendre ce partage possible

#### Versions de parcours

Action : 
- Concevoir le parcours sous la forme de modules dont il est possible de faire évoluer indépendamment les contenus. Il sera alors possible de retravailler de nouveaux parcours pour intégrer des versions différentes des modules adaptées au contexe d'une session de formation
Livrable :
- Définir les invariants du parcours et les "zones" d'adaptabilité à différents contextes
- Esquisser des points critiques de gouvernance sur la création de nouvelle version

#### Modèle économique 

Action :
- Penser la viabilité du partage en terme économique, c'est à dire intégrant : un temps d'animation de la communauté pédagogique, d'entretien de la ressource et de ses évolutions, de diffusion et contractualisation avec les tiers souhaitant mettre en oeuvre le parcours
Livrable :
- Evaluation du temps/coût par type de poste nécessaire à la viabilité du projet
- Etudier des scenarios de contractualisation avec des tiers

### Axe "Infrastructure pédagogique"

#### Plateforme LMS

Action & livrables : 
- Recenser les plateformes LMS existantes, leurs prises en charge des besoins identifiés ci-dessus, leurs coûts

#### Plateforme Open Badge

Action & livrables : 
- Recenser les plateformes OpenBadge existantes, leurs prises en charge des besoins identifiés ci-dessus, leurs coûts

#### Plateforme collaborative

Action & livrables : 
- Recenser les outils ou suite d'outils existante, leurs prises en charge des besoins identifiés ci-dessus, leurs coûts
