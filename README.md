# Parcours numérique éthique / low tech

Création d'un parcours de formation professionnelle destiné à sensibiliser les acteurs de l'éducation populaire et de l'animation sociale à la question de la transition écologique, des communs, de l'open source...

Cette première version est destinée à être donnée dans le cadre de la Fédération des Centres Sociaux du Nord Pas-de-Calais et le projet Centres Sociaux Connectés.

## Logique de parcours et modules distincts

Temps de formation en présentiel pour ouvir et clore la formation, associés à des contenus de formation à distance (modules).

Les modules abordent chacun une notion. Le format est simple, court et normé :
- Introduction
- Développement de la thématique autour d'exemples concrets
- Rappel des apports / définition / points clés
- Ressources pour aller plus loin / autres modules liés (hors parcours)
- Exercice à réaliser par le participant

L'idée générale est que les modules réalisés en autonomie ou en groupe autonome de participants ne demande pas plus de 30 minutes pour intégrer les "appports théoriques ou techniques" qui sont donnés dans l'introduction et les exemples concrets. Les exercices eux vont demander une plus forte implication de l'ordre de 1h à 3h.

Pour la correction des exercices, on préférera la "correction par les pairs", chaque participant reçoit la réalisation d'un autre participant au hasard et commente. 

On peut ajouter un temps de visio facultatif après chaque module pour échanger sur la thématique et/ou ouvrir un espace forum/chat pour y discuter.   

Au total, chaque module demandera environ de 2 à 4h d'implication.

Chaque module peut donner lieu à l'obtention d'un Open Badge (qui se basera sur la restitution des exercices de chaque module et éventuellement un QCM supplémentaire).

Le parcours ajoute une couche narrative sur le déroulé et relie les modules entre eux.
Il est constitué de :
- Une introduction "narrative" > [module] > [module] > [module] > .... > conclusion narrative

Dans la mesure du possible, les exercices/réalisations des modules sont réalisables indépendamment mais contribue dans leur ensemble à élaborer une réalisation unique à chaque participant.

## La thématique Low-Tech

Le concept de low-tech donne un cadre partagé et relie les problématiques posées d'impact écologique, lien social, réparabilité, efficience...
Cela peut donner au participant une "légitimité" à son parcours de formation et lui permettre de s'insérer dans un ensemble de réseau, partage de ressources plus large que le cadre de sa formation au sein des CS.

Le terme low-tech renvoie à une définition relativement bien stabilisée pour un terme relativement "jeune". Il est décrit par une certains nombre de pilliers qui varient d'une définition à l'autre mais reste consistent dans les modèles proposés.

A partir de :
- https://fr.wikipedia.org/wiki/Low-tech
- https://arte.wedodata.fr/lowtech/lowtech_fr/index_fr.html
- https://lowtechlab.org/fr/la-low-tech

on peut garder 6 points principaux en 2 parties qui constitueraient les 6 modules du parcours.

- Accessible
    - Simple et accessible à tous - [Module test](module1_1.md)
    - Economique (faible coût) - [Module test](module1_2.md)
    - DIYWO : que l'on peut faire soit même avec les autres - [Module test](module1_3.md)
- Durable
    - A faible impact écologique, à haute efficience - [Module test](module2_1.md)
    - Réaliser localement en utilisant des ressources locales - [Module test](module2_2.md)
    - Réparable - [Module test](module2_3.md)

**Note** :
Pour l'intitulé et le nom de la formation, la notion de numérique éthique est quand même très large. On peut restreindre à un champ plus restreint pour orienter transition (sociale, écologique, économique) ou directement low-tech ?


## Notions présentes dans le cursus

### Mot-clés
- Open source
- Documentation
- Fablab / Makers
- Fabrication distribuée
- Repair café / réparation
- Logiciel libre
- Obscolescence programmée
- DIY / DIYWO
- Réemploi
- Communs
- Pair à pair
- Circuit court
- Relocalisation

### Domaines
- Alimentation
- Energie
- Transport, mobilités
- Education
- Santé
- Habillement

### Formes
Initiatives de "transition" / de "sobriété-précarité" qui incarnent les intentions développées dans la formation, auxquelles on cherche à donner du sens dans un "système" à travers la formation :
- Jardins partagés, agriculture urbaine, initiatives de permaculture, cuisine partagée, conserverie...
- Fabrication de produits d'hygiène et d'entretien
- Achats groupés et/ou en circuits courts
- Ateliers d'auto-réparation ou d'aide à la réparation (vélo, couture, éléctroménager via les Repair cafés...)
- Fabrication distribuée, réparation en fablab
- Fabrication d'"appareils" low tech (garde manger, toilettes sèches,...)
- Solutions de mobilité partagée et/ou plus douce
- Pratiques de réemploi et d'upcycling, de don et de partage : vestiaire solidaire, gratiferia, prêt et mutualisation d'outils...
- Echanges de savoir-faire et de services : coiffeur solidaire...

## Points techniques et généraux

Le parcours est composé de module présentiel qui encadre des modules distanciels (à définir quand on aura stabilisé les modules et le périmètre je pense).

Les modules peuvent conduire à l'obtention d'un Open Badge.
Il est possible de construire une première version de ce parcours à travers la simple distribution de fichiers, sur un site web dédié existant à la fédération... et des badges "internes".

A terme, ou dès le démarrage, il est intéressant de connaître les outils qui pourraient être utilisé à terme.

### Plateforme LMS
Certains points techniques sont à déterminer au fur et à mesure de l'élaboration du programme :
- Choix d'une modalité pour les modules distanciels : [Moodle](https://moodle.org/?lang=fr), plateforme interne au CS ?

### Plateforme Open Badge
Choix d'une modalité pour les badges : :
- [Open Badge Factory](https://openbadgefactory.com),
- [Open Badge Passeport](https://openbadgepassport.com),
- [intégré à Moodle](https://moodle.org/?lang=fr)
...

Se rapprocher d'un fournisseur de badget existant pour mutualiser avec : 
- [Badgeons les hauts de france](https://www.c2rp.fr/actualites/badgeons-les-hauts-de-france)

### Utilisation de versioning
- Examiner / se rapprocher du modèle de [Et si j'accompagnais](https://www.etsijaccompagnais.fr)

### Rattachement à des référentiels existants
- Référentiel [Pix](https://pix.fr/competences/)
- Référentiel [DIGCOMP](https://europa.eu/europass/fr/how-describe-my-digital-skills)
- Créer un référentiel Low-Tech ?
