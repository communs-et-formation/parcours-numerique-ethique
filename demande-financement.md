
# Origine de la demande

La démarche Centres Sociaux Connectés accompagne la transformation du centre social avec le numérique et concourt à la montée en compétences des équipes, notamment salariées, dans le champ numérique entendu au sens large : éducation aux médias, culture numérique, pratiques collaboratives... De plus en plus, les préoccupations écologiques et démocratiques obligent à penser un numérique le plus éthique et responsable possible. De manière plus large, les articulations entre transition numérique et transition écologique necessitent un travail d'explication et d'illustration, notamment pour dépasser l'apparence contradictoire entre ces deux mouvements et pour intégrer la question démocratique comme pivot de ces transformations. 
En particulier, il est intéressant de montrer comment des notions issues de la culture numérique libre peuvent être des ingrédients d'une démarche de transition écologique basée sur l'émancipation, la coopération et l'autonomie à l'échelle locale. Ces notions sont fortement appropriées dans le milieu de "l'innovation sociale et numérique" mais peuvent sembler difficile d'accès pour une large partie de la population, voire être présentées comme des précoccupations réservées à certain.e.s. 
Open source, logiciels libres, communs, licences creative commons, DIY, low-tech, makers... Derrière des termes perçus comme techniques ou conceptuels, et le plus souvent anglo-saxons, se dessine un ensemble d'approches et de pratiques qui sont de nature à renforcer le pouvoir d'agir, individuel et collectif, sur son milieu de vie et ses conditions d'existence.

Pour accompagner l'appropriation de ces notions et des pratiques qui leur sont associées, la Fédération des Centres Sociaux du Nord Pas-de-Calais a initié la conception d'une formation modulaire à destination des salariés.e.s des centres sociaux.

# Objectif(s) visé(s)

Définir l’objectif à atteindre, notamment en termes de compétences ou de qualifications à acquérir. 
Mettre en évidence le caractère formatif de la formation et la transférabilité des compétences.

## Objectifs pédagogiques :

- 
- Favoriser la rencontre et les échanges entre les professionnel.le.s des centres sociaux du Nord Pas-de-Calais souhaitant mobiliser les pratiques et la culture numériques au service d'actions de transition écologique

## Objectifs d’apprentissage liés aux connaissances :

- Comprendre les termes du champ de l'innovation sociale et numérique et être en capacité de les associer à des pratiques mobilisées dans la sphère professionnelle de l'éducation populaire et de l'action sociale
- Comprendre comment les usages numériques et les pratiques issues de la culture numérique peuvent être mobilisées au service d'actions de transition écologique

## Objectifs d’apprentissage liés aux compétences :

- Développer sa capacité à expliquer et illustrer les liens entre pratiques numériques et transition écologique
- Développer sa capacité à donner sens et à accompagner des actions collectives accessibles, économes et durables pour répondre aux besoins du quotidien

